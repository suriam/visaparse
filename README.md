# viseca-parser

Parser to create a transactions XLS from viseca pdf statements.

You need to save all pdf invoices locally, the script will then convert those to plaintext using tika and parse each transaction to a new row in a pandas dataframe, which can then be saved in your preferred format (xls by default).

# Requirements

- Python >=3.9
- pandas, tika, openpyxl (pip install pandas tika openpyxl)

# Get started

- Login to one.viseca.ch portal
- On Invoices tab download all pdf invoices to ./input/ folder (get german language or adjust the search strings within script)
- Adjust paths at top of the script if necessary
- Run script (python visaparse.py)
- Check if number of lines in output xls matches your number of transactions
