import glob
import sys
import re
from pathlib import Path
import tika
from tika import parser  
tika.initVM()
import pandas as pd
from openpyxl import load_workbook

pdfFolder = "./input/*" # folder containing pdfs (must end in /*)
txtFolder = "./txt/" # target folder for text extraction files (must end in /)
outputXlsx = "./output/output2023.xlsx" # due to db error changed output to xlsx


class cTransaction:
    def __init__(self, datum="", valuta="", details1="", details2="", details3="", details4="", value="", foreignvalue="", transactiontype="", invoicedate="", credit=""):
        self.datum = ""
        self.valuta = ""
        self.details1 = ""
        self.details2 = ""
        self.details3 = ""
        self.details4 = ""
        self.value = ""
        self.foreignvalue = ""
        self.transactiontype = ""
        self.invoicedate = ""
        self.credit = ""    # 1 for true (for credit bookings like returns)


# Convert PDF to plaintext file
def parsePDF(pdfPath, txtPath):
    filePdf = Path(pdfPath)
    fileTxt = txtPath + filePdf.stem + ".txt"
    
    try: 
        parsed_pdf = parser.from_file(pdfPath)
    except FileNotFoundError:
        print("    File not found" + pdfPath)
        quit()
    
    contentPdf = parsed_pdf['content'] 
    
    # save content to textfile as is
    print("writing to " + fileTxt)
    with open(fileTxt, 'w') as f:
        f.write(contentPdf)
    

# Extract individual transactions from plaintext file
def parseTransactions(textToParse: str):
    # clean up the source file to only contain transactions:
    textToParseNoHeaderFooter = ""
    tupleToParseNoHeaderFooter = re.findall(r'Kartenlimite[\S\s]+?(?:Zwischensumme|Total Karte)', textToParse)
    for transBlock in tupleToParseNoHeaderFooter:
        textToParseNoHeaderFooter += transBlock + "\n"

    listText = textToParseNoHeaderFooter.splitlines() # convert string to list to iterate through lines

    transactionPagesStarts = [] # line numbers of beginning of each transaction section (one per pdf page)
    transactionPagesEnds = [] # line numbers of end...
    transactionSections = [] # will contain all transactions to separate (merged pages without header etc)
    invoiceSections = [] # contains invoice lines (totals etc)
    copyLine = False
    parsedTransactions = []

    invoicedate = re.search(r'Abrechnung vom (\d\d\.\d\d\.\d\d\d\d)\s', textToParse)[1]

    for lineIndex, lineText in enumerate(listText):
        match = re.search(r'(\d\d\.\d\d\.\d\d)\s*(\d\d\.\d\d\.\d\d)\s*(.*?)\b\s*([\d\.]+)\s*(-?)$', lineText)
        if match:
            print(f'Found transaction in line {lineIndex}: CHF {match[4]} on {match[1]} with text "{match[3]}"')
            credit = 1 if match[5] == '-' else 0
            newTransaction = cTransaction()
            newTransaction.date = match[1] 
            newTransaction.valuta = match[2]
            newTransaction.details1 = match[3]
            if credit == 1:
                newTransaction.value = -1 * float(match[4])
            else:
                newTransaction.value = match[4]
            
            newTransaction.invoicedate = invoicedate
            newTransaction.credit = credit
            parsedTransactions.append(newTransaction)

    return parsedTransactions
                

def convertPdfsToTxts(inputfolder, outputfolder):
    lfiles = glob.glob(inputfolder)     # cycle through folder containing pdf files to parse
    linefile = ''

    for fi in lfiles:       # append all text files to linefile
        print(f'parsing {fi}')
        parsePDF(fi, outputfolder)


def extractTransactionsFromTxts(inputfolder):
    allTransactions = []
    lfiles = glob.glob(inputfolder + '*')     # cycle through folder containing txt files to parse
    linefile = ''
    print("Plaintext files in list: ", lfiles)

    for fi in lfiles:  # append all text files to linefile
        print(f'parsing {fi}')
        with open(fi) as f:
            contents = f.read()
            allTransactions.extend(parseTransactions(contents))
    
    return allTransactions


def saveTransactionToExcel(transaction, excel_file):
    """Take transaction provided and write it to an Excel file"""
    
    # Create a DataFrame from the transaction
    data = {
        'date': [transaction.date],
        'valuta': [transaction.valuta],
        'details1': [transaction.details1],
        'details2': [transaction.details2],
        'details3': [transaction.details3],
        'details4': [transaction.details4],
        'value': [transaction.value],
        'foreignvalue': [transaction.foreignvalue],
        'transactiontype': [transaction.transactiontype],
        'invoicedate': [transaction.invoicedate],
        'credit': [transaction.credit],
    }
    df = pd.DataFrame(data)

    # Check if the Excel file already exists
    if Path(excel_file).is_file():
        # Load the existing Excel file
        with pd.ExcelWriter(excel_file, engine='openpyxl', mode='a', if_sheet_exists='overlay') as writer:
            # Append the data to the Excel file
            df.to_excel(writer, sheet_name='Transactions', index=False, header=False, startrow=writer.sheets['Transactions'].max_row)
    else:
        # Create a new Excel file
        df.to_excel(excel_file, sheet_name='Transactions', index=False)


def main():
    # initialize Transactions list
    allTransactions = []

    # first convert the downloaded pdf files
    convertPdfsToTxts(pdfFolder, txtFolder)
    
    # now parse plaintext files and collect transactions
    allTransactions = extractTransactionsFromTxts(txtFolder)

    # save all transactions to db/spreadsheet for further processing
    for transactionToSave in allTransactions:
        saveTransactionToExcel(transactionToSave, outputXlsx)


if __name__ == "__main__":
    sys.exit(main())
